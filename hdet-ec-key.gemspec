# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |spec|
  spec.name          = "hdet-ec-key"
  spec.version       = File.read("VERSION").strip
  spec.authors       = ["Stéphane Clérambault"]
  spec.email         = ["stephane@clerambault.fr"]

  spec.summary       = %q{Implements Herarchical Deterministic Key wallet.}
  spec.description   = %q{Provide an easy implementation of the BIP32
                          specification. This implementation is not related to
                          bitcoin and can be use for any propose.}
  spec.homepage      = "https://gitlab.com/elionne/hdet-ec-key"
  spec.license       = "MIT"

  spec.files         = [
    "lib/hdet-ec-key/data_manipulation.rb",
    "lib/hdet-ec-key/ec_manipulation.rb",
    "lib/hdet-ec-key/key.rb",
    "lib/hdet-ec-key.rb",
    "spec/bip32_vector_spec.rb",
    "spec/data_manipulation_spec.rb",
    "spec/key_spec.rb",
    "spec/test_vectors.rb",
    "LICENSE",
    "VERSION",
    "README.adoc"
  ]

  spec.extra_rdoc_files = ["README.adoc"]
  spec.require_paths = ["lib"]
  spec.required_ruby_version = '>= 2.5.0'

  spec.add_runtime_dependency "base58"
  spec.add_development_dependency "rspec"
end
