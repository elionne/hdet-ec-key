require 'openssl'
require 'digest'
require 'base58'

# Use for hardened index.
#
# To follow the notation about hardened index describe in BIP32 specification,
# you can use the method {#h} to return the corresponding hardened index.
#
# It also provide an easy way to write deviration path. For exemple:
#
#     "m/0/1h/10"
#     # can be write as
#     [0, 2147483649, 10]
#     # or more simply
#     [0, 1.h, 10]
#
# @return the self plus 2^31.
# @note https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki#extended-keys
class Integer
  def h
    2**31 + self
  end
end

module HDetEc
  autoload :DataManipulation, 'hdet-ec-key/data_manipulation'
  autoload :ECManipulation, 'hdet-ec-key/ec_manipulation'
  autoload :Key, 'hdet-ec-key/key'
end
