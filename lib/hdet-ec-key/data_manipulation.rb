
module HDetEc
  module DataManipulation

    # Serialize a 32-bit unsigned integer +i+ as a 4-byte sequence, most
    # significant byte first.
    def ser32(i)
      [i].pack("l>")
    end

    # Converts an integer to its binary represensation
    #
    # Returns a binary string that represents the binary form of the integer in
    # big-endian. A minimum length of byte can be specified, in this case the
    # result string while be a least +byte_length+ length. If the binary
    # representation need more byte, the +byte_length+ parameter is ignored.
    #
    # If the number need less bytes than specified, data will be rigth align and
    # padded with 0 ("\\x00") and +byte_length+ is respected.
    #
    # @param num [Integer, OpenSSL::BN] The input number to be converted.
    # @param bythe_length [Integer] the minimum size in byte of the output.
    #
    # @return [String] the converted integen into string.
    def to_binary(num, byte_length=1)
      num = num.to_i if num.kind_of? OpenSSL::BN

      hex_num = num.to_s(16).rjust(byte_length*2, "0")
      hex_num = "0" + hex_num if hex_num.size.odd?

      bin = [hex_num].pack("H*")
    end

    # Returns the number represented by the input binary string.
    #
    # @param bin [String] a binary string that represents an (big) Integer.
    # @return [Integer] the converted string as integer.
    def to_number(bin)
      bin.unpack1("H*").to_i 16
    end

    # Serializes the integer +p+ as a 32-byte sequence, most significant byte
    # first.
    def ser256(p)
      raise ArgumentError, "overflow" if p.bit_length > 256
      to_binary(p, 256/8)
    end

    def left_hash(h)
      h[0..31]
    end

    def right_hash(h)
      h[32..63]
    end

    def split_hash(h)
      [left_hash(h), right_hash(h)]
    end

    def rmd160_sha256(data)
      Digest::RMD160.digest Digest::SHA256.digest(data)
    end

    alias :hash160 :rmd160_sha256

    def double_sha256(data)
      Digest::SHA256.digest Digest::SHA256.digest(data)
    end

    alias :hash256 :double_sha256


    # Interprets a 32-byte sequence as a 256-bit number, most significant byte
    # first.
    alias :parse256 :to_number

  end
end
