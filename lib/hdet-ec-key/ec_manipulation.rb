require 'openssl'

module HDetEc
  module ECManipulation

    # The default group is the same as used by Bitcoin network, "secp256k1". But
    # it can be change, just provide the a OpenSSL group string.
    #
    # @example
    #  HDetEc::Key.group = "secp256r1"
    #
    def group=(openssl_group)
      @@group = OpenSSL::PKey::EC.new(openssl_group).group
      @@group.point_conversion_form = :compressed
      @@group
    end

    def group
      @@group ||= OpenSSL::PKey::EC.new("secp256k1").group
      @@group.point_conversion_form = :compressed
      @@group
    end

    # Generate public key from the private one.
    #
    # @param private_key [String] A binary string representation of the
    #   private key
    #
    # @return [String] The output is a binary representation of the public key
    #   in the compressed form.
    def generate_public_key_from_private(private_key)
      bn_private_key = OpenSSL::BN.new private_key, 2

      public_key = group.generator.mul(bn_private_key)
      public_key.to_bn.to_s(2)
    end

    alias_method :point, :generate_public_key_from_private

    # Generic derivation method for hardened keys
    #
    # Alogrithm to derive only hardened private key. That means no +index+
    # below 2^31 are allowed.
    def child_key_derivation_hardened(extended_key, index)
      k, c = extended_key
      key = c
      data = "\x00" + k + ser32(index)
      OpenSSL::HMAC.digest('SHA512', key, data)
    end

    alias_method :CKDh, :child_key_derivation_hardened

    # Generic derivation method for non-hardened keys.
    #
    # Alogrithm to derive public and private key if it is not an hardened key.
    # That means no +index+ above 2^31 - 1 are allowed.
    #
    # @param extended_key [Array] An extended key is an Array of two values. The
    #   first is the binary String of the key and the second is binary String of
    #   the chain code.
    # @param index [Integer] The choosen index for derivation key.
    def child_key_derivation(extended_key, index)
      k, c = extended_key
      key = c
      data = serp(k) + ser32(index)
      OpenSSL::HMAC.digest('SHA512', key, data)
    end

    alias_method :CKD, :child_key_derivation

    # Derive a public key according to BIP32 specifications
    #
    # Public parent key → public child key.
    #
    # @param extended_key [Array] (see {#child_key_derivation})
    # @param index [Integer] (see {#child_key_derivation})
    #
    # @note
    #   https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki#private-parent-key--private-child-key
    def child_key_derivation_private(extended_key, index)
      k, c = extended_key

      if index >= 2**31
        inter = child_key_derivation_hardened(extended_key, index)
      else
        inter = child_key_derivation([point(k), c], index)
      end

      iL, iR = split_hash(inter)

      bn = parse256(iL) + parse256(k)
      k_child = OpenSSL::BN.new(bn) % group.order

      [k_child.to_s(2), iR]
    end

    alias_method :CKDpriv, :child_key_derivation_private


    # Derive a public key according to BIP32 specifications
    #
    # Public parent key → public child key.
    #
    # @param extended_key [Array] (see {#child_key_derivation})
    # @param index [Integer] (see {#child_key_derivation})
    #
    # @note
    #   https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki#public-parent-key--public-child-key
    def child_key_derivation_public(extended_key, index)
      k, c = extended_key
      if index >= 2**31
        raise "No hardened public key derivation possible"
      end

      inter = child_key_derivation(extended_key, index)

      iL, iR = split_hash(inter)

      bn = OpenSSL::BN.new(iL, 2)
      pub_key = OpenSSL::PKey::EC::Point.new(group, k).mul(1, bn)
      [pub_key.to_octet_string(:compressed), iR]
    end

    alias_method :CKDpub, :child_key_derivation_public

    # Serializes the coordinate pair P = (x,y) as a byte sequence
    #
    # Serializes using SEC1's compressed form: (0x02 or 0x03) || ser256(x),
    # where the header byte depends on the parity of the omitted y coordinate.
    #
    # @param kp [String] public key as binary string.
    def serp(kp)
      # Ensure kp is in compressed form
      point = OpenSSL::PKey::EC::Point.new(group, kp)
      point.to_octet_string(:compressed)
    end

  end
end

