require 'hdet-ec-key'

require_relative 'test_vectors.rb'

shared_examples "public and private key derivation" do |vector|
  m = HDetEc::Key.generate_master_key([vector[:seed]].pack("H*"))
  vector[:data].each do |data|

    it data[:chain] + " private key" do
      expect(m.derive(data[:path]).serialize).to eq(data[:priv_key])
    end

    it data[:chain] + " public key" do
      expect(m.derive(data[:path]).public_key.serialize).to eq(data[:pub_key])
    end

    if data[:path].all? {|i| i < 2**31 }
      it data[:chain] + " public key derivation" do
        expect(m.public_key.derive(data[:path]).serialize).to eq(data[:pub_key])
      end
    end

  end
end

describe "derivation key seed = 000102030405060708090a0b0c0d0e0f" do
  vector = test_vector[0]
  include_examples("public and private key derivation", vector)
end

describe "derivation key seed = fffcf9f6f3f0edeae7e4e1...484542" do
  vector = test_vector[1]
  include_examples("public and private key derivation", vector)
end

describe "derivation key seed = 4b381541583be4...3235be" do
  vector = test_vector[2]
  include_examples("public and private key derivation", vector)
end
