require 'hdet-ec-key/data_manipulation'
require 'openssl'

describe HDetEc::DataManipulation do
  let(:dm) {Class.new {include HDetEc::DataManipulation}.new }

  context "#ser32" do
    it "serialize 4 bytes length" do
      expect(dm.ser32(0)).to eq("\x00\x00\x00\x00".b)
    end

    it "select least significant bytes when overflow" do
      expect(dm.ser32(2**32+2)).to eq("\x00\x00\x00\x02".b)
    end

    it "serialize in big-endian" do
      expect(dm.ser32(0xabcd)).to eq("\x00\x00\xab\xcd".b)
      expect(dm.ser32(0xabcd00ef)).to eq("\xab\xcd\x00\xef".b)
    end
  end

  context "#to_binary" do
    it "autodetect bytes length" do
      i2_38_33 = dm.to_binary(2**39 + 2**33)
      expect(i2_38_33.size).to be 5
      expect(i2_38_33).to eq("\x82\x00\x00\x00\x00".b)

      i2_15 = dm.to_binary(2**15)
      expect(i2_15.size).to be 2
      expect(i2_15).to eq("\x80\x00".b)
    end

    it "left pad with 0" do
      i2_33 = dm.to_binary(2**33)
      expect(i2_33).to eq("\x02\x00\x00\x00\x00".b)

      i2_127 = dm.to_binary(2**128)
      expect(i2_127).to eq("\x01".b + "\x00".b * 16)
    end

    it "serialize in big-endian" do
      expect(dm.to_binary(0x0123abcdef)).to eq("\x01\x23\xab\xcd\xef".b)
      expect(dm.to_binary(0xabcd)).to eq("\xab\xcd".b)
    end

    it "could specify serialize byte size" do
      expect(dm.to_binary(0, 8).size).to eq 8
      expect(dm.to_binary(2**128-1, 2).size).to eq 16
    end
  end

  context "#to_number" do
    it "could convert number" do
      expect(dm.to_number("\x01".b + "\x00".b * 16)).to eq(2**128)
    end

    it "deserialize in big-endian" do
      expect(dm.to_number("\x01\x23\xab\xcd\xef")).to eq(0x0123abcdef)
      expect(dm.to_number("\xab\xcd")).to eq(0xabcd)
    end
  end

  context "#ser256" do
    it "always 256 bits long (32 bytes)" do
      expect(dm.ser256(1).size).to eq(32)
      expect(dm.ser256(73927273964184847435527778358743529074551849469743120849858998083583072341153).size).to eq(32)
    end

    it "raise ArgumentError exception when overflow" do
      expect{dm.ser256(2**256)}.to raise_error(ArgumentError)
    end

    it "serialize in big-endian" do
      expect(dm.ser256(0x0123abcdef)).to eq( "\x00".b * 27 + "\x01\x23\xab\xcd\xef".b)
    end
  end

end
