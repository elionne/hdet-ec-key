require 'hdet-ec-key'

describe HDetEc::Key do
  context "#import" do
    it "can import private key from BIP32 string" do
      bip32_key = "xprv9z4pot5VBttmtdRTWfWQmoH1taj2axGVzFqSb8C9xaxKymcFzXBDptWmT7FwuEzG3ryjH4ktypQSAewRiNMjANTtpgP4mLTj34bhnZX7UiM"
      key = HDetEc::Key.import(bip32_key)

      expect(key.serialize).to eq(bip32_key)
    end

    it "can import public key from BIP32 string" do
      bip32_key = "xpub6D4BDPcP2GT577Vvch3R8wDkScZWzQzMMUm3PWbmWvVJrZwQY4VUNgqFJPMM3No2dFDFGTsxxpG5uJh7n7epu4trkrX7x7DogT5Uv6fcLW5"
      key = HDetEc::Key.import(bip32_key)

      expect(key.serialize).to eq(bip32_key)
    end
  end
end
